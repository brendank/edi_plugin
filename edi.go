package edi

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"

	"golang.org/x/net/html"

	"github.com/PuerkitoBio/goquery"
	"github.com/asaskevich/govalidator"
	"github.com/mmcdole/gofeed"

	"github.com/davecgh/go-spew/spew"
	"github.com/itsabot/abot/shared/datatypes"
	"github.com/itsabot/abot/shared/plugin"
)

type JSONView struct {
	Related []struct {
		Type string `json:"type"`
	} `json:"related"`
}

// Pages is...
type Pages struct {
	Pages []Page
}

// Page is a recommendation
type Page struct {
	Path string `json:"path"`
}

// Recommendations is a list of recommendations
type Recommendations struct {
	Recommendations []Recommendation `json:"recommendations"`
}

// Recommendation is a recommendation
type Recommendation struct {
	URLCanonical string `json:"urlcanonical"`
}

var p *dt.Plugin

func init() {
	var err error
	p, err = plugin.New("bitbucket.org/brendank/edi_plugin")
	if err != nil {
		log.Fatal(err)
	}
	plugin.SetKeywords(p,
		dt.KeywordHandler{
			Fn: kwGetTrending,
			Trigger: &dt.StructuredInput{
				Commands: []string{"what", "show", "tell"},
				Objects:  []string{"hot"},
			},
		},
		dt.KeywordHandler{
			Fn: kwGetDevelopment,
			Trigger: &dt.StructuredInput{
				Commands: []string{"find", "what", "show", "tell"},
				Objects:  []string{"develop"},
			},
		},
		dt.KeywordHandler{
			Fn: kwGetOpportunities,
			Trigger: &dt.StructuredInput{
				Commands: []string{"find", "what", "show", "tell"},
				Objects:  []string{"opportunity"},
			},
		},
		dt.KeywordHandler{
			Fn: kwGetImprovements,
			Trigger: &dt.StructuredInput{
				Commands: []string{"how"},
				Objects:  []string{"improved"},
			},
		},
		dt.KeywordHandler{
			Fn: kwNextStory,
			Trigger: &dt.StructuredInput{
				Commands: []string{"show", "tell", "find"},
				Objects:  []string{"next"},
			},
		},
		dt.KeywordHandler{
			Fn: kwRelatedStory,
			Trigger: &dt.StructuredInput{
				Commands: []string{"show", "tell", "find"},
				Objects:  []string{"related"},
			},
		},
		dt.KeywordHandler{
			Fn: kwGetRural,
			Trigger: &dt.StructuredInput{
				Commands: []string{"what", "find", "tell", "show"},
				Objects:  []string{"rural"},
			},
		},
	)

	if err = plugin.Register(p); err != nil {
		p.Log.Fatal(err)
	}
}

func kwNextStory(in *dt.Msg) (resp string) {
	var pagelist Pages
	curr, _ := strconv.Unquote(string(p.GetMemory(in, "currentpageidx").Val))
	curridx, _ := strconv.Atoi(curr)
	pll, _ := strconv.Unquote(string(p.GetMemory(in, "pagelistlen").Val))
	pagelistlen, _ := strconv.Atoi(pll)
	spew.Dump(curridx)
	err := json.Unmarshal(p.GetMemory(in, "pagelist").Val, &pagelist)
	if err != nil {
		return "Unable to find more stories"
	}
	if curridx+1 == pagelistlen {
		return "No more stories"
	}
	newarticle := pagelist.Pages[curridx+1].Path
	p.SetMemory(in, "currentpageidx", curridx+1)
	p.SetMemory(in, "currentpage", newarticle)
	return "This article has high engagement but not many people are viewing it.\n" + newarticle

}

func kwGetImprovements(in *dt.Msg) (resp string) {
	listtype, _ := strconv.Unquote(string(p.GetMemory(in, "listtype").Val))
	spew.Dump(listtype)
	if listtype == "developments" {
		return findDevelopments(in)
	}
	if listtype == "opportunities" {
		return findOpportunities(in)
	}
	return "Sorry, can't think of anything."
}

func findDevelopments(in *dt.Msg) (resp string) {
	var jv JSONView
	var relatedArticleCount, relatedImageCount, relatedVideoCount int
	article, _ := strconv.Unquote(string(p.GetMemory(in, "currentpage").Val))
	articleid := getArticleID(article)

	r, err := http.Get(strings.Replace(article, "http://mobile", "http://www", 1))
	if err != nil {
		return ""
	}
	defer r.Body.Close()
	articleStr, err := ioutil.ReadAll(r.Body)
	if err == nil {
		counters := parseBodyHTML(string(articleStr))

		resp = resp + "This article has:\n"
		resp = resp + "* " + strconv.Itoa(counters["subHeadingCount"]) + " subheadings\n"
		resp = resp + "* " + strconv.Itoa(counters["blockQouteCount"]) + " block quotes\n"
		resp = resp + "* " + strconv.Itoa(counters["inlineImageCount"]) + " inline image\n"
		resp = resp + "* " + strconv.Itoa(counters["infoGraphicCount"]) + " infographics\n"
		resp = resp + "* " + strconv.Itoa(counters["videoCount"]) + " inline videos\n"
		resp = resp + "* " + strconv.Itoa(counters["audioCount"]) + " inline audio\n"
		resp = resp + "* " + strconv.Itoa(counters["paraCount"]) + " paragraphs\n"
		resp = resp + "* " + strconv.Itoa(counters["galleryCount"]) + " inline galleries\n"
		resp = resp + "* " + strconv.Itoa(counters["pullQuoteCount"]) + " inline pull quotes\n"
		resp = resp + "* " + strconv.Itoa(counters["textEmbedCount"]) + "inline text embeds\n"
	}

	r2, err := http.Get("http://www.abc.net.au/news/ajax/news/" + articleid + "/json;")
	if err != nil {
		return ""
	}
	defer r2.Body.Close()
	jsonviewStr, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return ""
	}

	_ = json.Unmarshal(jsonviewStr, &jv)

	spew.Dump(jv)
	for _, related := range jv.Related {
		if related.Type == "article" {
			relatedArticleCount++
		}
		if related.Type == "image" {
			relatedImageCount++
		}
		if related.Type == "video" {
			relatedVideoCount++
		}
	}

	resp = resp + "* " + strconv.Itoa(relatedArticleCount) + " related articles\n"
	resp = resp + "* " + strconv.Itoa(relatedVideoCount) + " related video\n"
	resp = resp + "* " + strconv.Itoa(relatedImageCount) + " related images\n"

	resp = resp + "\nMake story longer with stronger images, video options, related links placed before they leave the page, and make sure to offer the best story hook or quotes at the top of your story."
	return resp
}

func findOpportunities(in *dt.Msg) (resp string) {
	article, _ := strconv.Unquote(string(p.GetMemory(in, "currentpage").Val))
	articleid := getArticleID(article)

	TopStoriesIdx := findInFeed("http://www.abc.net.au/news/feed/45910/rss.xml", articleid)
	FeaturedIdx := findInFeed("http://www.abc.net.au/news/feed/5516574/rss.xml", articleid)

	if TopStoriesIdx < 0 {
		resp = resp + "The article is not in Top Stories.\n"
	} else {
		resp = resp + "The article is in position " + strconv.Itoa(TopStoriesIdx) + " of Top Stories.\n"
	}
	if FeaturedIdx < 0 {
		resp = resp + "The article is not in Featured.\n"
	} else {
		resp = resp + "The article is in position " + strconv.Itoa(FeaturedIdx) + " of Featured.\n"
	}
	return resp + "Increase the article's profile by adding to or moving up Top Stories/Featured bulletins. Consider publishing socially on Facebook or Twitter."
}

func findInFeed(feedURL, articleid string) int {
	fp := gofeed.NewParser()
	feed, err := fp.ParseURL(feedURL)
	spew.Dump(feed)
	if err != nil {
		panic(err)
	}
	for i, item := range feed.Items {
		if strings.Index(item.Link, articleid) >= 0 {
			return i
		}
	}
	return -1
}

func getArticleID(article string) string {
	exp := "http:\\/\\/.*\\/([0-9]+)$"
	re := regexp.MustCompile(exp)
	a := re.FindStringSubmatch(article)
	if len(a) < 2 {
		return ""
	}
	return a[1]
}

func kwGetTrending(in *dt.Msg) (resp string) {
	var recs Recommendations
	p.Log.Debug("getting trending")
	r, err := http.Get("https://recommendations.abc.net.au/api/v1/recommendations?limit=5&models=trending-movavg&app_id=News&apikey=e9dd0ac7-16e5-4762-b67f-53af5b5490da")
	if err != nil {
		return ""
	}
	p.Log.Debug("decoding resp")
	if err = json.NewDecoder(r.Body).Decode(&recs); err != nil {
		fmt.Println("unable to parse response")
		return ""
	}
	if err = r.Body.Close(); err != nil {
		return ""
	}
	if len(recs.Recommendations) == 0 {
		return "Nothing is trending at the moment"
	}
	p.Log.Debug("got trending")

	return recs.Recommendations[0].URLCanonical
}

func kwGetRural(in *dt.Msg) (resp string) {
	var recs Pages
	p.Log.Debug("getting development")
	fmt.Println("getting development")
	r, err := http.Get("http://localhost:3000/rural")
	if err != nil {
		return ""
	}
	p.Log.Debug("decoding resp")
	if err = json.NewDecoder(r.Body).Decode(&recs); err != nil {
		return ""
	}
	if err = r.Body.Close(); err != nil {
		return ""
	}
	if len(recs.Pages) == 0 {
		return "Can't find anything :("
	}
	p.Log.Debug("got rural")

	resp = "These articles are smashing it in Rural at the moment:\n"
	for i, rec := range recs.Pages {
		resp = resp + "* " + rec.Path + "\n"
		if i == 5 {
			break
		}
	}
	return resp
}

func kwRelatedStory(in *dt.Msg) (resp string) {
	article, _ := strconv.Unquote(string(p.GetMemory(in, "currentpage").Val))
	articleid := getArticleID(article)
	var recs Recommendations
	p.Log.Debug("getting trending")
	r, err := http.Get("https://recommendations.abc.net.au/api/v1/recommendations?limit=5&models=morelikethis&app_id=News&apikey=e9dd0ac7-16e5-4762-b67f-53af5b5490da&context=coremedia://article/" + articleid)
	if err != nil {
		return ""
	}
	p.Log.Debug("decoding resp")
	if err = json.NewDecoder(r.Body).Decode(&recs); err != nil {
		fmt.Println("unable to parse response")
		return ""
	}
	if err = r.Body.Close(); err != nil {
		return ""
	}
	if len(recs.Recommendations) == 0 {
		return "Sorry, can't find anything similar at the moment"
	}
	p.Log.Debug("got trending")

	resp = resp + "These articles seem similar:\n"

	for _, rec := range recs.Recommendations {
		resp = resp + "* " + rec.URLCanonical + "\n"
	}

	return resp
}

func kwGetDevelopment(in *dt.Msg) (resp string) {
	var recs Pages
	p.Log.Debug("getting development")
	fmt.Println("getting development")
	r, err := http.Get("http://localhost:3000/high-con-low-eng")
	if err != nil {
		return ""
	}
	p.Log.Debug("decoding resp")
	if err = json.NewDecoder(r.Body).Decode(&recs); err != nil {
		return ""
	}
	if err = r.Body.Close(); err != nil {
		return ""
	}
	if len(recs.Pages) == 0 {
		return "Everything looks pretty good at the moment. Make a cup to tea."
	}
	p.Log.Debug("got trending")

	p.SetMemory(in, "listtype", "developments")
	p.SetMemory(in, "pagelist", recs)
	p.SetMemory(in, "pagelistlen", len(recs.Pages))
	p.SetMemory(in, "currentpageidx", 0)
	p.SetMemory(in, "currentpage", recs.Pages[0].Path)
	return "This article is delivering a big audience, but they are on the page for less than 30 seconds.\n" + recs.Pages[0].Path
}

func kwGetOpportunities(in *dt.Msg) (resp string) {
	var recs Pages
	p.Log.Debug("getting development")
	r, err := http.Get("http://localhost:3000/low-con-high-eng")
	if err != nil {
		return ""
	}
	p.Log.Debug("decoding resp")
	if err = json.NewDecoder(r.Body).Decode(&recs); err != nil {
		return ""
	}
	if err = r.Body.Close(); err != nil {
		return ""
	}
	if len(recs.Pages) == 0 {
		return "Everything looks pretty good at the moment. Make a cup to tea."
	}
	p.Log.Debug("got trending")

	p.SetMemory(in, "listtype", "opportunities")
	p.SetMemory(in, "pagelist", recs)
	p.SetMemory(in, "pagelistlen", len(recs.Pages))
	p.SetMemory(in, "currentpageidx", 0)
	p.SetMemory(in, "currentpage", recs.Pages[0].Path)
	return "This article has high engagement but not many people are viewing it.\n" + recs.Pages[0].Path
}

func parseBodyHTML(htmlStr string) map[string]int {
	fmt.Println(htmlStr)
	var externalLinkCount, subHeadingCount, blockQouteCount, inlineImageCount, infoGraphicCount, videoCount, audioCount, paraCount, galleryCount, pullQuoteCount, textEmbedCount int
	doc, _ := goquery.NewDocumentFromReader(bytes.NewBufferString(htmlStr))

	sel := doc.Find("div.article.section")

	// For links...
	sel.Find("a").Each(func(j int, link *goquery.Selection) {
		// Strip links that are not just plain text links
		if link.ChildrenFiltered("img").Length() > 0 {
			link.ReplaceWithSelection(link.ChildrenFiltered("img").Remove())
		}
		// make links absolute
		if href, hasHref := link.Attr("href"); hasHref {
			re := regexp.MustCompile("^/news/")
			link.SetAttr("href", re.ReplaceAllString(href, "http://www.abc.net.au/news/"))
			externalLinkCount++
		}
		// Strip those that don't have valid hrefs
		if href, hasHref := link.Attr("href"); hasHref {
			if !govalidator.IsRequestURL(href) {
				linkContents, _ := link.Html()
				link.ReplaceWithHtml(linkContents)
			}
		}
	})

	// remove any empty tags
	//sel.Find("bold:empty, strong:empty, em:empty, i:empty").ReplaceWithHtml("")
	sel.Find("bold, strong, em, i, u").Each(func(j int, el *goquery.Selection) {
		if el.Is(":empty") {
			el.ReplaceWithHtml("")
		} else if regexp.MustCompile(`\s*`).ReplaceAllString(el.Text(), "") == "" {
			el.ReplaceWithHtml(" ")
		}
	})

	// Clean up any paragraphs...
	sel.ChildrenFiltered("p,h1,h2,h3,h4").Each(func(j int, para *goquery.Selection) {
		// ...that have images (sometimes the images are wrapped in a paragraph for some reason)
		if para.ChildrenFiltered("img").Length() > 0 {
			para.PrependSelection(para.ChildrenFiltered("img"))
			para.ReplaceWithSelection(para.ChildrenFiltered("img").Remove())
		}
		paraCount++
		// ...that are empty
		if strings.TrimSpace(para.Text()) == "" {
			para.Remove()
			paraCount--
		}
	})

	sel.Contents().Each(func(i int, s *goquery.Selection) {
		fmt.Println(s.Get(0).Data)
		switch {
		case s.Get(0).Type == html.TextNode, s.Get(0).Data == "p", s.Get(0).Data == "ul", s.Get(0).Data == "ol", s.Is(`a[href]`):
			//
		case s.Get(0).Data == "h4", s.Get(0).Data == "h2":
			subHeadingCount++
		case s.Get(0).Data == "h5", s.Get(0).Data == "h3":
			subHeadingCount++
		case s.Get(0).Data == "blockquote":
			blockQouteCount++
		case s.Get(0).Data == "img":
			inlineImageCount++
		case s.Get(0).Data == "br":
			// do nothing for breaks
		case s.First().Is("a:not([href])"):
			// do nothing for components that are just named anchors (we can't use them)
		case s.Get(0).Data == "div":
			switch {
			case s.Is(".inline-content.photo,.inline-content.infographic"):
				infoGraphicCount++
			case s.Is(".inline-content.video"):
				videoCount++
			case s.Is(".inline-content.audio"):
				audioCount++
			case s.Is(".inline-content.gallery"):
				galleryCount++
			case s.Is(".inline-content.quote"):
				pullQuoteCount++
			case s.Is(".inline-content.wysiwyg"):
				textEmbedCount++
			case s.Is(".inline-content.html-fragment"):
				//
			case s.Is(".inline-content") && s.Find("strong").First().Text() == "External Link:":
				externalLinkCount++
			default:
				//
			}
		default:
			//
		}
	})
	return map[string]int{
		"embedLinkCount":    externalLinkCount,
		"externalLinkCount": externalLinkCount,
		"subHeadingCount":   subHeadingCount,
		"blockQouteCount":   blockQouteCount,
		"inlineImageCount":  inlineImageCount,
		"infoGraphicCount":  infoGraphicCount,
		"videoCount":        videoCount,
		"audioCount":        audioCount,
		"paraCount":         paraCount,
		"galleryCount":      galleryCount,
		"pullQuoteCount":    pullQuoteCount,
		"textEmbedCount":    textEmbedCount,
	}
}
